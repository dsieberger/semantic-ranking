import numpy as np
import matplotlib.pyplot as plt

from scipy.linalg import norm

from warnings import simplefilter
from scipy.cluster.hierarchy import linkage
from scipy.cluster.hierarchy import cut_tree
from scipy.cluster.hierarchy import dendrogram
from scipy.cluster.hierarchy import ClusterWarning

from .utils import angular_distances

class Clustering:
    
    @staticmethod
    def hierarchy(vectors, method='single'):
        simplefilter('ignore', ClusterWarning)
        distance_matrix = angular_distances(vectors)
        return linkage(distance_matrix, method=method)
    
    @staticmethod
    def clusters(hierarchy, height=50):
        return cut_tree(hierarchy, height=height)
    
    @staticmethod
    def centroids(vectors, clusters, min_pts=30):
        centroids = []
        for cluster in np.unique(clusters):
            matches = np.any(clusters == cluster, axis=1)
            indices = [i for i, v in enumerate(matches) if v]
            matching_vectors = vectors[indices]
            if matching_vectors.shape[0] >= min_pts:
                average = np.average(matching_vectors, axis=0)
                centroids.append(np.divide(average, norm(average)))
        return np.array(centroids)
    
    @staticmethod
    def dendrogram(hierarchy, height=50):
        plt.figure(figsize=(20,6))
        dendrogram(hierarchy, color_threshold=height, no_labels=True)
        plt.title('Dendrogram of Agglomeraive clustering for document embeddings')
        plt.ylabel('Distance')
        plt.show()
        