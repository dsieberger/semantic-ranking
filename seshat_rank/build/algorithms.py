import math
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity

# Calculates the angular distance between vectors in the embedding manifold
def angular_distances(X, Y):
    return np.arccos(np.clip(np.inner(X, Y), -1, 1))/np.pi

# HYPOTHESIS 1
# Determine the average similarity of the most 'semantically similar' X number of
# embeddings for each book. Rank books in descending order by this average.
# Interpretation -> A book that is more biased towards the query topic,
# the closer some subset of token embeddings will group around the query vector representation.
# PROS:
# - Takes into account the density of book embeddings surrounding a "topic" (quantifiable).
# - The algorithm execution is limited to a subset of embeddings and executes quickly (fast).
# CONS:
# - It is not as accurate in dealing with books of considerable different sizes. More embeddings
#   translate to an overall denser distribution of datapoints. This directly interferes with the
#   density measurement.
def max_avg_similarity(encoded_query, embeddings, embedding_count=300):
    assert embeddings.shape[0] > 0
    assert 0 < embedding_count
    distances = np.sort(angular_distances(encoded_query, embeddings))
    nearest_vectors = distances[:min(embedding_count, distances.shape[0] - 1)]
    similarity = 1 - np.average(nearest_vectors) # 1 - distance = similarity
    return similarity

# HYPOTHESIS 2
# Determine the average similarity of the most 'semantically similar' X% of
# embeddings for a given book.
# PROS:
# - Takes into account the density of book embeddings surrounding a "topic" (quantifiable).
# - The algorithm execution is limited to a subset of embeddings and executes quickly (fast).
# - Slightly mitigates the density problem that 'max_avg_similarity' suffers from different sized books.
# CONS:
# - It is still not offering an entirely accurate similarity score towards the "topic" when dealing
#   with books with very different sizes.
def normalized_max_avg_similarity(encoded_query, embeddings, embedding_ratio=0.2):
    assert embeddings.shape[0] > 0
    assert 0 < embedding_ratio < 1
    distances = np.sort(angular_distances(encoded_query, embeddings))
    n_nearest = math.ceil(embedding_ratio * distances.shape[0])
    nearest_vectors = distances[:n_nearest]
    similarity = 1 - np.average(nearest_vectors) # 1 - distance = similarity
    return similarity

# HYPOTHESIS 3
# Raise the "importance" of nearby embeddings while lowering it for those further away.
# Given the higher clustering of distances close to the 0.5 distance value, due to maximal surface area of
# the embedding space (hypersphere), we want to focus more on what is happening near the query embedding.
# Both 'max_avg_similarity' and 'normalized_max_avg_similarity' have the issue of its score being "pulled"
# towards the mean embedding distance of 0.5.
def weighted_proximity_score(encoded_query, embeddings, bias_exponent=0.3, range_coefficient=0.5):
    assert embeddings.shape[0] > 0
    assert bias_exponent > 0
    assert 0 < range_coefficient <= 1
    distances = angular_distances(encoded_query, embeddings)
    distances = distances[distances < range_coefficient]
    weight_fn = lambda x: (1 - x/range_coefficient)**bias_exponent
    weights = np.array([weight_fn(x) for x in distances])
    weighted_proximity = np.multiply(weights, distances)
    score = np.average(weighted_proximity)
    return score


# HYPOTHESIS 4
# From the premise that the set of embedding distances to the encoded query follow a normal
# distribution, the average distance for any continuous interval of distances will have a bias
# towards the mean distance of the entire data set. One may aleviate this bias by subsampling
# embeddings, ordered by their distance, with a stochastic method that mimics a uniform distribution
# (i.e. lowering the bell curve of the embedding distance distribution).
# def stochastic_max_avg_similarity(encoded_query, embeddings, sample_size=300):
#     assert embeddings.shape[0] > 0
#     assert 0 < sample_size
#     distances = np.sort(angular_distances(encoded_query, embeddings))
#     epsilon = np.average([distances[i] - distances[i - 1] for i in range(1, embeddings.shape[0])])
#     sampling_distances = np.random.uniform(0, 1, sample_size)
#     sampled_embeddings
#     for sample in sampling:
#     	for i, distance in enumerate(distances):
#     		if sample - epsilon < distance or sample + epsilon > distance:




###### OTHERS ######
# def knn(self, vector, k:int):
#     distances = angular_distances([vector], self._embeddings)
#     return self._tokens[list(distances[0].argsort()[:k])]

# # breaks features' distributions into quantiles
# # used to compare distances in quantiles between documents
# def fingerprint1(self, precision=20):
#     quantiles = np.linspace(0, 1, num=precision)
#     return np.quantile(self._embeddings, quantiles, axis=0).T

# # parametric fit of a skewed normal distribution using Maximum Likelihood Estimation
# # while precision with quantiles method can vary based on the number of quantiles
# # with this function, there are only 3 necessary features to plot the estimated
# # distribution: (shape, location, scale) -> ("skewness", mean, standard deviation)
# def fingerprint2(self):
#     return np.apply_along_axis(skewnorm.fit, 0, self._embeddings)
#     #skewnorm.rvs(*params, size=3000)
###### END ######
