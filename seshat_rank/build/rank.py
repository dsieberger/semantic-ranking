import numpy as np
import algorithms

import os
import sys
import time
import base64
import argparse
from kafka import KafkaConsumer, KafkaProducer
from kafka.errors import KafkaConnectionError, NoBrokersAvailable
from pymongo import MongoClient
from bson import json_util
import gridfs

# Low process priority
# os.nice(1)

# Standard output config
sys.stdout.reconfigure(line_buffering=True)

# Parse command line parameters
parser = argparse.ArgumentParser(description='Seshat library ranking algorithms.')
parser.add_argument('--bootstrap-servers', type=str, help='the Kafka broker connection: <broker-1>[,<broker-n>]')
parser.add_argument('--database-conn', type=str, help='the database server url')
parser.add_argument('--store-memory', action='store_true', help='the database server url')
parser.add_argument('--topic-in', type=str, help='the Kafka topic from which the messages are received')
parser.add_argument('--topic-out', type=str, help='the Kafka topic from which the messages are received')
args = parser.parse_args()

# Database connection
mongodb = MongoClient(args.database_conn)

# Connect to Kafka network
def kafka_connect(bootstrap_servers):
    global producer
    global consumer
    kafka_connected = False
    print('Connecting to Kafka bootstrap_servers: %s' % (bootstrap_servers))
    while not kafka_connected:
        try:
            producer = KafkaProducer(bootstrap_servers=bootstrap_servers, acks=0)
            consumer = KafkaConsumer(args.topic_in,
                bootstrap_servers=bootstrap_servers,
                group_id='seshat-rank')
                # enable_auto_commit=False,
                # auto_offset_reset='earliest')
            kafka_connected = True
            print('Successfully connected to the Kafka network!')
        except NoBrokersAvailable:
            time.sleep(3)
            print('Kafka connection failed. Reattempting to connect...')

kafka_connect(args.bootstrap_servers)

with mongodb.start_session() as session:
    db = session.client.seshat
    fs = gridfs.GridFS(db)

    # Keep database in memory for faster response time
    mem_db = {}
    if args.store_memory:
        books = db.books.find(filter={ 'encoded_representation': { '$exists': True } })
        for book in list(books):
            encoded_book = fs.get(book['encoded_representation']).read()
            embeddings = np.frombuffer(encoded_book, dtype=np.float32)
            embeddings = embeddings.reshape(-1, 512)
            mem_db[book['_id']] = embeddings

    print('Ready.')
    for message in consumer:
        try:
            data = json_util.loads(message.value)

            if not args.store_memory:
                # Find book's embeddings
                book = db.books.find_one(
                    filter={ '_id': data['book_id'] },
                    projection={ 'encoded_representation': True })

                # Parse into numpy arrays
                encoded_book = fs.get(book['encoded_representation']).read()
                embeddings = np.frombuffer(encoded_book, dtype=np.float32)
                embeddings = embeddings.reshape(-1, 512)
            else:
                embeddings = mem_db[data['book_id']]

            encoded_query = base64.b64decode(data['encoded_query'])
            encoded_query = np.frombuffer(encoded_query, dtype=np.float32)

            ### SCORING ###
            similarity = -1
            if hasattr(algorithms, data['algorithm']):
                fn = getattr(algorithms, data['algorithm'])
                similarity = fn(encoded_query, embeddings)
                if np.isnan(similarity):
                    raise ValueError('Similarity score for book "%s" is NaN.' % (data['book_id']))
            else:
                raise ValueError('Empty reference for ranking algorithm: "%s".' % (data['algorithm']))

            # Broadcast result to the Kafka Network
            producer.send(args.topic_out, json_util.dumps({
                'request_ID': data['request_ID'],
                'book_id': data['book_id'],
                'similarity': str(similarity)
            }).encode('utf-8'))
            # producer.flush()
            # consumer.commit_async()

        except KafkaConnectionError:
            print('Lost connection to Kafka.')
            kafka_connect(args.bootstrap_servers)
        except ValueError as e:
            print(e)
            # consumer.commit_async()
        except Exception as e:
            print(e)
            # consumer.commit_async()
