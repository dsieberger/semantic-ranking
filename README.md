# Semantic-based Document Ranking
This project is the implementation of a "smart librarian" system which we call **Seshat**. It deploys a series of microservices that communicate with each other as components in a pre-processing pipeline, or online querying service. These services communicate with each other via Kafka messaging system and processed corpus data is stored in a MongoDB database.

## Microservices
- _mongodb_: Persistent storage within the cluster. Composed of processed corpus data.
- _kafka_: Messaging service that handles communication between the different services and generalizes over the individual service instances.
- _seshat_parser_: The input corpus is fed through a Google Cloud bucket API. An authentication file is required to be in its root folder in order to process the documents stored off-premises. The documents are parsed so that their textual content is tokenized into sentences via regular expression matching. Each sentence is stored in _mongodb_, and then dispatched via _kafka_ for the next stage of the pipeline.
- _seshat_encoder_: Receives text sentences propagated by _seshat_parser_ and encodes them with the **Universal Sentence Encoder** model. The resulting embedding is stored in _mongodb_. 
- _seshat_query_: Node.js server instance that listens on a websocket interface for query requests. Parameters include the search term and the ranking algorithm. It dispatches tuples containing the encoded search query, ranking algorithm, and a document ID belonging to the processed corpus over _kafka_ for _seshat_rank_ instances to work. It awaits for the document scores and redirects the results back to the client.
- _seshat_rank_: Fetches the associated embeddings from _mongodb_ for the document matching the input ID and performs the chosen ranking algorithm. The resulting score is sent back to _seshat_query_, along with the original request data.
- _seshat_client_: React.js web client with GUI interface to select search parameters and display the resulting ranked permutation.

## Kubernetes deployment
This project uses Helm to handle the Kubernetes applications/microservices within the cluster. A set of scripts are made available for easier deployment and shutdown of the services. Two scripts are also provided for backing up and restoring database information regarding the processed corpus data.

