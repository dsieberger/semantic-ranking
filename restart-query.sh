#!/bin/bash
helm delete seshat-query
docker build -t registry.gitlab.com/dsieberger/thesis/seshat_query seshat_query/build/
docker push registry.gitlab.com/dsieberger/thesis/seshat_query
helm install seshat-query seshat_query/helm -f seshat_query/helm/values.yaml