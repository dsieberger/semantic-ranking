#!/bin/bash
# # Install Helm
# curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
# sudo apt-get install apt-transport-https --yes
# echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
# sudo apt-get update
# sudo apt-get install helm
# # Add bitnami repo
# helm repo add bitnami https://charts.bitnami.com/bitnami
# helm repo update
# Deploy cluster
# helm install kafka bitnami/kafka -f kafka/values.yaml
# helm install mongodb bitnami/mongodb -f mongodb/values.yaml
helm install seshat-parser seshat_parser/helm -f seshat_parser/helm/values.yaml
helm install seshat-encoder seshat_encoder/helm -f seshat_encoder/helm/values.yaml
# helm install seshat-rank seshat_rank/helm -f seshat_rank/helm/values.yaml
# helm install seshat-query seshat_query/helm -f seshat_query/helm/values.yaml
# helm install seshat-client seshat_client/helm -f seshat_client/helm/values.yaml
