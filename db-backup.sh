#!/bin/bash
mongod > /dev/null 2>&1 &
kubectl port-forward --namespace default svc/mongodb 27016:27017 > /dev/null 2>&1 &

TIMESTAMP=$(date "+%Y.%m.%d-%H.%M.%S")
mkdir ./.db/$TIMESTAMP
mongodump --host=127.0.0.1 --port=27016 --username=seshat --password=seshat-8719 --db=seshat -o ./.db/$1/$TIMESTAMP/
kill -9 $!
