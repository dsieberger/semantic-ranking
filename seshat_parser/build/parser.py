import io
import os
import re
import sys
import time
import json
import threading
import argparse

import nltk
import pdftotext
from bs4 import BeautifulSoup
from epub_conversion.utils import open_book, convert_epub_to_lines
from kafka import KafkaProducer
from kafka.errors import KafkaConnectionError, NoBrokersAvailable

from bson import json_util
from pymongo import MongoClient, ReturnDocument
from pymongo.errors import DuplicateKeyError, BulkWriteError

from google.auth import jwt
from google.cloud import pubsub_v1
from google.cloud import storage

nltk.download('punkt')

# Low process priority
os.nice(1)

# Standard output config
sys.stdout.reconfigure(line_buffering=True)

# Parse command line parameters
parser = argparse.ArgumentParser(description='Tokenize and stream Seshat library books.')
parser.add_argument('--bootstrap-servers', type=str, help='the Kafka broker connection: <broker-1>[,<broker-n>]')
parser.add_argument('--database-conn', type=str, help='the database connection string <protocol>://<username>:<password>@<address>:<port>')
parser.add_argument('--topic-in', type=str, default='seshat-library-sub', help='the Kafka topic from which the messages are received')
parser.add_argument('--topic-out', type=str, default='seshat-parser', help='the Kafka topic into which the messages are sent')
args = parser.parse_args()

# Database connection
mongodb = MongoClient(args.database_conn)

# Google config
PROJECT_ID = 'seshat-wtot'
CONFIG_FILE = 'seshat-wtot-8ffc8a794d4e.json'
BUCKET_ID = 'seshat-library-7282'

# Open file stream from file path and retrieve text
def read(file_name:str, raw_bytes):
    # Read EPUB files
    if file_name.lower().endswith('.epub'):
        book = open_book(raw_bytes)
        lines = convert_epub_to_lines(book)
        text = []
        for line in lines:
            soup = BeautifulSoup(line, 'html.parser')
            content = soup.find_all(text=True)
            text.append(''.join(content))
        return ''.join(text)
    # Read PDF files
    elif file_name.lower().endswith('.pdf'):
        pages = pdftotext.PDF(raw_bytes)
        return ''.join(pages)
    # Read TXT files
    elif file_name.lower().endswith('.txt'):
        return raw_bytes.read().decode('utf-8')
    else:
        return raw_bytes.read().decode('utf-8')
    # else: raise 'Unable to read file.'

# Break up input text into sentence tokens
def tokenize(content:str):
    assert type(content) == str
    assert len(content) > 0
    return [
        re.sub(r'\s+', ' ', s).strip()
        for s in nltk.tokenize.sent_tokenize(content)
    ]

# Sanitize sentence tokens
def sanitize(tokens):
    MIN_LEN = 15
    MAX_LEN = 1000
    CHAR_RATIO_MIN_LOWER_BOUND = 0.8
    CHAR_RATIO_MAX_LOWER_BOUND = 0.9
    # discard sentences with length outside of range [15-1000]
    print('Total number of tokens: %d' % len(tokens))
    tokens = list(filter(lambda t: len(t) >= MIN_LEN, tokens))
    print('...Excluding short tokens (length < %d): %d' % (MIN_LEN, len(tokens)))
    tokens = list(filter(lambda t: len(t) <= MAX_LEN, tokens))
    print('...Excluding long tokens (length > %d): %d' % (MAX_LEN, len(tokens)))
    # discard sentences with low character-to-non-character ratio [below 80%-90%]
    pattern = re.compile(r'[^\w\s]|[\d]')
    min_char_ratio = lambda t: (
        (len(pattern.sub('', t)) / len(t)) >
        (
            CHAR_RATIO_MAX_LOWER_BOUND - (CHAR_RATIO_MAX_LOWER_BOUND - CHAR_RATIO_MIN_LOWER_BOUND) *
            MIN_LEN * (
                (len(t) - (MIN_LEN + MAX_LEN))**2 /
                (len(t) * MAX_LEN**2)
            )
        )
    )
    tokens = list(filter(min_char_ratio, tokens))
    print('...Excluding tokens with word character density ratio (below %d%%-%d%%): %d' %
        (CHAR_RATIO_MIN_LOWER_BOUND*100, CHAR_RATIO_MAX_LOWER_BOUND*100, len(tokens)))
    return tokens

# Connect to Kafka network
def kafka_connect(bootstrap_servers):
    global producer
    kafka_connected = False
    print('Connecting to Kafka brokers: %s' % (bootstrap_servers))
    while not kafka_connected:
        try:
            producer = KafkaProducer(
                bootstrap_servers=bootstrap_servers,
                acks='all')
            kafka_connected = True
            print('Successfully connected to the Kafka network!')
        except NoBrokersAvailable:
            time.sleep(3)
            print('Kafka connection failed. Reattempting to connect...')

kafka_connect(args.bootstrap_servers)

# Stream sentence tokens into Kafka message system
def callback(message):
    try:
        data = json.loads(message.data)
        book_id = data['id']
        book_name = data['name']

        with mongodb.start_session() as session:
            db = session.client.seshat

            # Save book metadata into persistent storage
            book = db.books.insert_one({
                '_id': book_id,
                'name': book_name,
                'n_tokens': -1,
                'encoded_tokens': 0,
                'fully_encoded': False
            })

            # Download file stream
            print('Parsing: %s' % (book_name))
            blob = bucket.get_blob(book_name)
            data_stream = io.BytesIO(blob.download_as_bytes())

            # Parse book into tokens
            content = read(book_name, data_stream)
            tokens = tokenize(content)
            sanitized_tokens = sanitize(tokens)

            # Save parsed tokens into persistent storage
            token_rows = db.tokens.insert_many([{
                '_id': book_id + '/' + str(i),
                'book_id': book_id,
                'index': i,
                'token': token
            } for i, token in enumerate(sanitized_tokens)])

            # Save book metadata into persistent storage
            db.books.update_one({
                '_id': book.inserted_id
            }, { '$set': { 'n_tokens': len(sanitized_tokens) } })

        # Broadcast to Kafka network
        for id in token_rows.inserted_ids:
            producer.send(args.topic_out, json_util.dumps(id).encode('utf-8'))
        producer.flush()
        message.ack()

    except DuplicateKeyError:
        print('Duplicated book entry found: %s' % (data['id']))
        message.ack()
    except BulkWriteError as e:
        print('Error in storing tokens: %s' % (e))
        message.ack()
    except KafkaConnectionError:
        print('Lost connection to Kafka.')
        kafka_connect(args.bootstrap_servers)
        message.nack()
    except Exception as e:
        print(e)
        message.nack()

# Google PubSub
audience = "https://pubsub.googleapis.com/google.pubsub.v1.Subscriber"
credentials = jwt.Credentials.from_service_account_info(
    json.load(open(CONFIG_FILE)), audience=audience
)
subscriber = pubsub_v1.SubscriberClient(credentials=credentials)
subscription_name = 'projects/{project_id}/subscriptions/{sub}'.format(
    project_id=PROJECT_ID,
    sub=args.topic_in  # Set this to something appropriate.
)

# Google Storage
storage_client = storage.Client.from_service_account_json(CONFIG_FILE)
bucket = storage_client.get_bucket(BUCKET_ID)

# Notification listener
future = subscriber.subscribe(subscription_name, callback)

print('Ready.')
future.result()
