#!/bin/bash
helm delete seshat-encoder
docker build -t registry.gitlab.com/dsieberger/thesis/seshat_encoder seshat_encoder/build/
docker push registry.gitlab.com/dsieberger/thesis/seshat_encoder
helm install seshat-encoder seshat_encoder/helm -f seshat_encoder/helm/values.yaml
