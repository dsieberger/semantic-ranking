import React from 'react';
import { Row } from './Row';

interface TableProps {
  headers: Array<string>
}
interface TableState {
  results: Array<Record<string, string>>
}
export class Table extends React.Component<TableProps, TableState> {

  constructor(props: any) {
    super(props);
    this.state = {
      results: []
    };
  }

  updateResults(data: Record<string, string>): void {
    let results = [...this.state.results, data];
    results.sort((a, b) => {
      if (a.similarity > b.similarity) return -1;
      else if (a.similarity < b.similarity) return 1;
      else return 0;
    });
    this.setState({ results: [] });
    this.setState({ results: results });
  }

  componentDidMount(): void {}

  render(): JSX.Element {
    return (
      <table id="document-list" className="ui single line striped selectable unstackable table">
        <thead>
          <tr>{ this.props.headers.map((header: string) => {
            return (<th key={ header }>{ header }</th>);
          }) }</tr>
        </thead>
        <tbody>
          { this.state.results.map((result: Record<string, any>, i: number) => {
            return (<Row key={ i } data={ result } />);
          }) }
         </tbody>
      </table>
    );
  }
};