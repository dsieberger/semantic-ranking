import React from 'react';

interface RowProps {
  data: Record<string, any>
}
interface RowState {
  book: string,
  score: number
}
export class Row extends React.Component<RowProps, RowState> {
  constructor(props: RowProps) {
    super(props);
    this.state = {
      book: props.data.book_id,
      score: props.data.similarity
    }
  }

  componentDidMount(): void {}

  render(): JSX.Element {
    return (
      <tr>
        <td data-name='name'>{ this.state.book.split('/')[1] }</td>
        <td data-name='score'>{ this.state.score }</td>
      </tr>
    );
  }
};