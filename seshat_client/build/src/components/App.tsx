import React from 'react';
import { Table } from './Table';
import { Search } from './Search';
import { Dropdown, Menu, Container, Image, DropdownProps } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
const Websocket: any = require('react-websocket');

const algorithms = [{
  key: 'normalized_max_avg_similarity',
  text: 'normalized_max_avg_similarity',
  value: 'normalized_max_avg_similarity'
}, {
  key: 'max_avg_similarity',
  text: 'max_avg_similarity',
  value: 'max_avg_similarity'
}, {
  key: 'weighted_proximity_score',
  text: 'weighted_proximity_score',
  value: 'weighted_proximity_score'
}];

interface AppProps {}
interface AppState {
  timestamp: string,
  queryText: string,
  algorithm: string,
  refWebsocket: React.RefObject<any>,
  refTable: React.RefObject<any>
}
export default class App extends React.Component<AppProps, AppState> {

  constructor(props: any) {
    super(props);
    this.state = {
      timestamp: '',
      queryText: '',
      algorithm: 'normalized_max_avg_similarity',
      refWebsocket: React.createRef(),
      refTable: React.createRef()
    };
  }

  updateAlgorithm(e: React.SyntheticEvent<HTMLElement, Event>, data: DropdownProps): void {
    this.setState({
      algorithm: data.value as string
    });
  }

  handleQuery(queryText: string): void {
    this.setState({
      timestamp: (new Date()).toISOString(),
      queryText: queryText
    }, () => {
      this.state.refWebsocket.current.sendMessage(JSON.stringify({
        requestID: this.state.timestamp + '||' +
                   this.state.algorithm + ':' + this.state.queryText,
        queryText: this.state.queryText,
        algorithm: this.state.algorithm
      }));
    });
  }

  handleResults(data: string): void {
    let newResult = JSON.parse(data);
    let currentRequest = this.state.timestamp + '||' +
        this.state.algorithm + ':' + this.state.queryText
    if (newResult.request_ID === currentRequest)
      this.state.refTable.current.updateResults(newResult);
  }

  componentDidMount(): void {}

  render(): JSX.Element {
    return (
      <Container>
        <Menu>
          <Menu.Item>
            <Image src='/img/logo2.png' size='small'></Image>
          </Menu.Item>
            <Dropdown
              item
              simple
              text='Algorithm'
              defaultValue={ this.state.algorithm }
              options={ algorithms }
              onChange={ this.updateAlgorithm.bind(this) }
            />
              <Menu.Item className='right'>
                <Search handleQuery={ this.handleQuery.bind(this) } />
              </Menu.Item>
        </Menu>
        <main className="ui container">
          <Table key={ this.state.timestamp } headers={['Book', 'Score']}
            ref={ this.state.refTable }/>
          <Websocket url='ws://10.102.182.248:8000/'
            onMessage={ this.handleResults.bind(this) }
            ref={ this.state.refWebsocket }/>
        </main>
      </Container>
    );
  }
};
