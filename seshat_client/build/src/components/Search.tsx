import React from 'react';

interface SearchProps {
  handleQuery: Function
}
interface SearchState {
  queryText: string
}
export class Search extends React.Component<SearchProps, SearchState> {

  constructor(props: any) {
    super(props);
    this.state = {
      queryText: ''
    };
  }

  updateQueryText(e: React.ChangeEvent<HTMLInputElement>): void {
    this.setState({ queryText: e.target.value });
  }

  componentDidMount(): void {}

  render(): JSX.Element {
    return (
      <div id="search-query" className="ui action left icon input">
        <i className="search icon"></i>
        <input type="text" placeholder="Search..." value={ this.state.queryText }
               onChange={ this.updateQueryText.bind(this) } />
        <div className="ui teal button"
             onClick={() => { this.props.handleQuery(this.state.queryText); }}>Search</div>
      </div>
    );
  }
}
