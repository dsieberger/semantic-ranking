#!/bin/bash
helm delete seshat-parser
docker build -t registry.gitlab.com/dsieberger/thesis/seshat_parser seshat_parser/build/
docker push registry.gitlab.com/dsieberger/thesis/seshat_parser
helm install seshat-parser seshat_parser/helm -f seshat_parser/helm/values.yaml
