import sys
import base64
import numpy as np
import tensorflow as tf
import tensorflow_hub as hub

# model = hub.load('model')
model = hub.load('https://tfhub.dev/google/universal-sentence-encoder-large/5')
print('MODEL_LOADED')

for line in sys.stdin:
	try:
		query_text = line.strip()
		embedding = model([query_text])[0]
		encoded_embedding = embedding.numpy().tobytes()
		encoded_embedding = base64.b64encode(encoded_embedding)
		print('success:' + str(encoded_embedding, 'utf-8'))
	except Exception as e:
		print(e)
