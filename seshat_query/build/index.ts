import path from 'path';
import assert from 'assert';
import { Promise as promise } from 'bluebird';
import WebSocket from 'ws';
import { Kafka } from 'kafkajs';
import { MongoClient } from 'mongodb';
import _ from 'lodash';
import { spawn, ChildProcessWithoutNullStreams } from 'child_process';

var subprocess: ChildProcessWithoutNullStreams;
var dbClient: MongoClient;

(async () => {

  var argv = require('minimist')(process.argv.slice(2));
  const wss = new WebSocket.Server({ port: 8000 });
  var connections: Record<string, WebSocket> = {};

  // Load USE model
  subprocess = spawn('python3', [
    '-u', path.join(__dirname, 'load_model.py')]);
  subprocess.stdout.on('data', (data) => {
    if (data.toString('utf-8').indexOf('MODEL_LOADED') > -1)
      console.error(`${data}`);
  });
  subprocess.stderr.on('data', (data) => {
    console.error(`python_err: ${data}`);
  });
  subprocess.stderr.on('close', () => {
    console.log("Closing python process...");
  });

  // MONGODB
  var mongo: MongoClient = new MongoClient(
    argv['database-conn'],
    { useUnifiedTopology: true }
  );
  dbClient = await mongo.connect();
  console.log('MongoDB: successfully connected');
  var db = dbClient.db('seshat')
  var collection = await db.collection('books');
  var library: Array<any>;
  const pollLibrary = async () => {
    library = await collection.find({
      fully_encoded: true
      // encoded_tokens: { $gte: 300 }  // FOR BENCHMARKING
    }).toArray();
    console.log('Library contains:', library.length, 'books.');
  };
  pollLibrary(); // Poll library for books and update manifest every 5 minutes.
  setInterval(pollLibrary, 300000);

  // KAFKA
  const kafka = new Kafka({
    clientId: 'seshat-query',
    brokers: [argv['bootstrap-servers']]
  });
  const producer = kafka.producer();
  const consumer = kafka.consumer({ groupId: 'seshat-query' });
  await producer.connect();
  await consumer.connect();
  await consumer.subscribe({ topic: argv['topic-in'] });
  await consumer.run({
    partitionsConsumedConcurrently: 1,
    eachMessage: async ({ topic, partition, message }) => {
      try {
        let value = message.value?.toString();
        let data = JSON.parse(value as string);
        let client = connections[data.request_ID];
        if (!!client && client.readyState === WebSocket.OPEN) {
          client.send(JSON.stringify(data));
        }
      } catch(e) { console.error('seshat-query received a faulty message:', e); }
    }
  });

  // Websocket listener
  wss.on('connection', async (ws: WebSocket) =>
  {
    // Limit max socket connections / delete older one
    if (Object.keys(connections).length == 20) {
      let timestamps = Object.keys(connections);
      let oldest_timestamp = timestamps.sort()[0];
      let client = connections[oldest_timestamp]
      if (client.readyState === WebSocket.OPEN)
          client.terminate();
      delete connections[oldest_timestamp];
    }

    ws.setMaxListeners(200);
    ws.on('close', () => {
      console.log('Client disconnected.');
    });
    // Receive a new query
    ws.on('message', async (message: WebSocket.Data) =>
    {
      // Parse search query and encode it
      let parameters = JSON.parse(message.toString());
      // Cancel previous request
      Object.keys(connections).map((key) => {
        if (connections[key] === ws) {
          delete connections[key];
        }
      });

      // Add new request entry
      let requestID = (new Date()).toISOString() + '||' +
          parameters.algorithm + ':' + parameters.queryText;
      connections[parameters.requestID || requestID] = ws;

      // Encode query with USE
      subprocess.stdin.write(parameters.queryText + '\n');

      // Prepare Kafka producer messages
      subprocess.stdout.on('data', async (data: any) => {
        const flag = 'success:';
        data = data.toString('utf-8').trim();
        if (data.length > flag.length && data.slice(0, flag.length)) {
          var np_array = data.slice(flag.length);
          if (data.startsWith(flag)) {
            await producer.send({
              topic: argv['topic-out'],
              messages: library.map((book: any) => {
                return {
                  value: JSON.stringify({
                    book_id: book._id,
                    encoded_query: np_array,
                    algorithm: parameters.algorithm,
                    request_ID: parameters.requestID || requestID
                  })
                }
              })
            });
          }
        }
      });
    });
  });

})().catch((err) => {
  console.error(err);
  dbClient.close();
});
