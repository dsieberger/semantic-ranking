import os
import sys
import time
import argparse
import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
from kafka import KafkaConsumer
from kafka.errors import NoBrokersAvailable
from pymongo import MongoClient, ReturnDocument
from bson import json_util
import gridfs

# Low process priority
# os.nice(1)

# Standard output config
sys.stdout.reconfigure(line_buffering=True)

# Parse command line parameters
parser = argparse.ArgumentParser(description='Encode Seshat library tokens.')
parser.add_argument('--bootstrap-servers', type=str, help='the Kafka broker connection: <broker-1>[,<broker-n>]')
parser.add_argument('--database-conn', type=str, help='the database connection string <protocol>://<username>:<password>@<address>:<port>')
parser.add_argument('--topic-in', type=str, default='seshat-parser', help='the Kafka topic from which the messages are received')
args = parser.parse_args()

# Database connection
mongodb = MongoClient(args.database_conn)

print('Loading Tensorflow model...')
# model = hub.load('model')
model = hub.load('https://tfhub.dev/google/universal-sentence-encoder-large/5')
print('Model loaded.')

# Connect to Kafka network
def kafka_connect(bootstrap_servers):
	global consumer
	kafka_connected = False
	print('Connecting to Kafka bootstrap server: %s' % (bootstrap_servers))
	while not kafka_connected:
		try:
			consumer = KafkaConsumer(args.topic_in,
				bootstrap_servers=bootstrap_servers,
				group_id='seshat-encoder',
				enable_auto_commit=False,
				auto_offset_reset='earliest')
			kafka_connected = True
			print('Successfully connected to the Kafka network!')
		except NoBrokersAvailable:
			time.sleep(3)
			print('Kafka connection failed. Reattempting to connect...')


kafka_connect(args.bootstrap_servers)

with mongodb.start_session() as session:
	db = session.client.seshat
	fs = gridfs.GridFS(db)
	print('Ready.')
	for message in consumer:
		try:
			# Encode sentence token into embedding
			objectId = json_util.loads(message.value)
			entry = mongodb.seshat.tokens.find_one({
				'_id': objectId,
				'embedding': { '$exists': False }
			})
			if entry is None:
				consumer.commit_async()
				continue
			embedding = model([entry['token']])[0]
			encoded_embedding = embedding.numpy().tobytes()

			# Save embedding to persistent storage via transaction
			with session.start_transaction():

				# Store token embedding
				stored_embedding = fs.put(encoded_embedding)
				token = db.tokens.find_one_and_update({ '_id': objectId }, {
					'$set' : { 'embedding': stored_embedding }
				}, return_document=ReturnDocument.BEFORE)

				# If token wasn't yet encoded, check if the book has embeddings missing
				if 'embedding' not in token:
					book = db.books.find_one_and_update({ '_id': token['book_id'] }, {
						'$inc': { 'encoded_tokens': 1 },
					}, return_document=ReturnDocument.AFTER)

					# Mark book as being fully encoded
					if book['n_tokens'] == book['encoded_tokens']:
						all_book_tokens = db.tokens.find({
							'book_id': book['_id'],
							'embedding': { '$exists': True }
						})
						all_encoded_embeddings = [
							fs.get(t['embedding']).read()
							for t in all_book_tokens
						]
						encoded_book = np.concatenate([
							np.frombuffer(e, dtype=np.float32)
							for e in all_encoded_embeddings
						], axis=0)
						encoded_book = encoded_book.tobytes()
						stored_encoded_book = fs.put(encoded_book)
						db.books.update_one({ '_id': book['_id'] }, {
							'$set': {
								'fully_encoded': True,
								'encoded_representation': stored_encoded_book
							}
						})

			consumer.commit_async()

		except Exception as e:
			print(e)
