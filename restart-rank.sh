#!/bin/bash
helm delete seshat-rank
docker build -t registry.gitlab.com/dsieberger/thesis/seshat_rank seshat_rank/build/
docker push registry.gitlab.com/dsieberger/thesis/seshat_rank
helm install seshat-rank seshat_rank/helm -f seshat_rank/helm/values.yaml