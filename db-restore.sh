#!/bin/bash
mongod > /dev/null 2>&1 &
kubectl port-forward --namespace default svc/mongodb 27016:27017 > /dev/null 2>&1 &
mongorestore --host=127.0.0.1 --port=27016 --username=seshat --password=seshat-8719 --db=seshat --dir=./.db/$1
kill -9 $!
# mongo mongodb://seshat:seshat-8719@127.0.0.1:27016/seshat
